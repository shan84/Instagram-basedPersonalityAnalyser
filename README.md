<h1 align="center"> Instagram-based Personality Analyser </h1>
<p align="center">
</p>
<br>
![](https://s-media-cache-ak0.pinimg.com/564x/32/e0/c4/32e0c456f0d1802d35d625459a03309a.jpg)
![](https://s-media-cache-ak0.pinimg.com/originals/09/0f/06/090f066e1bfcc3f0cf9f25313e50824f.png)
<p align="center">
</p>

*Still Beta Version!*
## Supporting this project
Instagram-based Personality Analyser is a personal interest project developed by a [ university student](https://www.linkedin.com/in/shanhuangg/) from the University of Manchester.Its release and ongoing development thanks to the support by his teammates. You can help improve this project better by supporting me via [gitlab](https://gitlab.com/shan84/InstagramAnalyser). Thank you!   

## Introduction
This tool provide an idea by analysing Instagram users posts, we can get most relevant interest about them. **E.g. what they like to eat, what is their favourite colour, etc.**
Feel free to ask questions, post issues, submit pull request, and request new features.

For more information about this project and how to use this extension, please check out our documentation ⬇︎

## Documentation
To check out the documentation, visit
* [English](https://docs.google.com/document/d/1ATN2gNnPgC8JyOrSv5TdsseDtHq04_59Dcph5vumLe0/edit?usp=sharing)
* [简体中文](https://docs.google.com/document/d/10jUmQ9D15a4qXTggj5MyrE2iL9hg7fJcaHtV76yvQa0/edit?usp=sharing)

Contact me if you are willing to help translate the documentation :)


## License
[The Unlicense License](https://gitlab.com/shan84/InstagramAnalyser/blob/master/LICENSE)  
